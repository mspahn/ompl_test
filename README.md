## Installation

```bash
mkdir build
cd build
cmake ..
make
```


Then you will see the executables in the build folder.
