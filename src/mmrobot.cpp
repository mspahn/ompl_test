/********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2010, Rice University
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Rice University nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Mark Moll */

#include <ompl/base/spaces/DubinsStateSpace.h>
#include <ompl/base/spaces/ReedsSheppStateSpace.h>
#include <ompl/base/ScopedState.h>
#include <ompl/geometric/SimpleSetup.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <boost/program_options.hpp>

namespace ob = ompl::base;
namespace og = ompl::geometric;
namespace po = boost::program_options;

// The easy problem is the standard narrow passage problem: two big open
// spaces connected by a narrow passage. The hard problem is essentially
// one long narrow passage with the robot facing towards the long walls
// in both the start and goal configurations.
//
bool isInBox(double x, double y)
{
  // obstacle box
  double x_low = 0, x_up = 0.5, y_low = -1.5, y_up = -0.5;
  return (x > x_low && x < x_up && y > y_low && y < y_up);
}

bool isInBox1(double x, double y)
{
  // obstacle box
  double x_low = -1.5, x_up = -1, y_low = 1.5, y_up = 2.5;
  return (x > x_low && x < x_up && y > y_low && y < y_up);
}
  
bool isStateValidMine(const ob::SpaceInformation *si, const ob::State *state)
{
//  const auto *cs = dynamic_cast<ob::CompoundStateSpace*>(state);
  const auto *cs = state->as<ob::SE2StateSpace::StateType>();
//  const auto *s = cs->getSubspace(0)->as<ob::SE2StateSpace::StateType<>();
  double x = cs->getX(), y = cs->getY();
  //return x < 0.5;
  return !(isInBox(x, y) || isInBox1(x, y));
  //return true;
}

bool isStateValidEasy(const ob::SpaceInformation *si, const ob::State *state)
{
    const auto *s = state->as<ob::SE2StateSpace::StateType>();
    double x=s->getX(), y=s->getY();
    return si->satisfiesBounds(s) && (x<5 || x>13 || (y>8.5 && y<9.5));
}

void plan(const ob::StateSpacePtr& space)
{

    ob::ScopedState<> start(space), goal(space);

    // define a simple setup class
    og::SimpleSetup ss(space);
    bool easy = true;

    // set state validity checking for this space
    //const ob::SpaceInformation *si = ss.getSpaceInformation().get();
    const ob::SpaceInformation *si = ss.getSpaceInformation().get();
    auto isStateValid = easy ? isStateValidMine : isStateValidMine;
    ss.setStateValidityChecker([isStateValid, si](const ob::State *state)
        {
            return isStateValid(si, state);
        });

    // set the start and goal states
    start[0] = 0.0;
    start[1] = 0.0;
    //start[2] = 0.0;
    //start[3] = -0.5;
    goal[0] = 0.0;
    goal[1] = 2.0;
    //goal[2] = 2.0;
    //goal[3] = 0.3;
    ss.setStartAndGoalStates(start, goal);

    // this call is optional, but we put it in to get more output information
    ss.getSpaceInformation()->setStateValidityCheckingResolution(0.005);
    ss.setup();
    ss.setPlanner(std::make_shared<og::RRTConnect>(ss.getSpaceInformation()));
    ss.print();

    // attempt to solve the problem within 30 seconds of planning time
    ob::PlannerStatus solved = ss.solve(30.0);

    if (solved)
    {
        std::vector<double> reals;

        std::cout << "Found solution:" << std::endl;
        ss.simplifySolution();
        og::PathGeometric path = ss.getSolutionPath();
        //path.interpolate(1000);
        path.printAsMatrix(std::cout);
    }
    else
        std::cout << "No solution found" << std::endl;
}

int main(int argc, char* argv[])
{
    try
    {
        // Declae a ReedsSheppStateSpace
        ob::StateSpacePtr baseSpace(std::make_shared<ob::DubinsStateSpace>(4));
        std::cout << "Is symmetric : " << baseSpace->as<ob::SE2StateSpace>()->hasSymmetricDistance() << std::endl;
        ob::StateSpacePtr armSpace(new ob::RealVectorStateSpace(7));

        // Set bounds
        ob::RealVectorBounds boundsBase(2);
        boundsBase.setLow(-5);
        boundsBase.setHigh(5);
        baseSpace->as<ob::SE2StateSpace>()->setBounds(boundsBase);
        ob::RealVectorBounds boundsArm(7);
        boundsArm.setLow(-1);
        boundsArm.setHigh(1);
        armSpace->as<ob::RealVectorStateSpace>()->setBounds(boundsArm);


        ob::StateSpacePtr space = baseSpace + armSpace;
        //ob::CompoundStateSpace *space = new ob::CompoundStateSpace();
        //
        

        // Declare a DubinsStateSpace (no moving backward)
        //ob::StateSpacePtr space = std::make_shared<ob::DubinsStateSpace>();
        
        plan(baseSpace);
        const std::vector<double> trajectoryPoint = std::vector<double>({1.0, 2.0, 3.0});
        //printTrajectory(space, trajectoryPoint);
    }
    catch(std::exception& e) {
        std::cerr << "error: " << e.what() << "\n";
        return 1;
    }
    catch(...) {
        std::cerr << "Exception of unknown type!\n";
    }

    return 0;
}
